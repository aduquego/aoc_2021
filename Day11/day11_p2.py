from itertools import count, product

def neighbors(r, c, h, w):
	deltas = (
		(1, 0), (-1,  0), ( 0, 1), ( 0, -1),
		(1, 1), ( 1, -1), (-1, 1), (-1, -1)
	)

	for dr, dc in deltas:
		rr, cc = (r + dr, c + dc)
		if 0 <= rr < h and 0 <= cc < w:
			yield rr, cc

def energy(dumbo_octupuses, r, c, h, w):
	if dumbo_octupuses[r][c] <= 9:
		return

	dumbo_octupuses[r][c] = -1

	for nr, nc in neighbors(r, c, h, w):
		if dumbo_octupuses[nr][nc] != -1:
			dumbo_octupuses[nr][nc] += 1
			energy(dumbo_octupuses, nr, nc, h, w)

def steps(dumbo_octupuses, h, w):
	flashes = 0

	for r, c in product(range(h), range(w)):
		dumbo_octupuses[r][c] += 1

	for r, c in product(range(h), range(w)):
		energy(dumbo_octupuses, r, c, h, w)

	for r, c in product(range(h), range(w)):
		if dumbo_octupuses[r][c] == -1:
			dumbo_octupuses[r][c] = 0
			flashes += 1

	return flashes

with open(r'input_p1.txt', 'r') as f:
    lines = map(str.rstrip, f)
    dumbo_octupuses= list(list(map(int, row)) for row in lines)

h, w    = len(dumbo_octupuses), len(dumbo_octupuses[0])
n_cells = h * w

sum_flashes = sum(steps(dumbo_octupuses, h, w) for _ in range(100))

for sol in count(101):
	if steps(dumbo_octupuses, h, w) == n_cells:
		break

print(sol)
