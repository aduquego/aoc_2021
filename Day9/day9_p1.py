
def neighbors(r, c, h, w):
	for dr, dc in ((1, 0), (-1, 0), (0, 1), (0, -1)):
		rr, cc = (r + dr, c + dc)
		if 0 <= rr < h and 0 <= cc < w:
			yield (rr, cc)

with open(r'input_p1.txt', 'r') as f:
    lines = map(str.rstrip, f)
    grid  = tuple(tuple(map(int, row)) for row in lines)
    h, w  = len(grid), len(grid[0])

sol = 0

for r, row in enumerate(grid):
	for c, cell in enumerate(row):
		if all(grid[nr][nc] > cell for nr, nc in neighbors(r, c, h, w)):
			sol += cell + 1

print(sol)
