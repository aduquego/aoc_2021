output = []
x = 0
y = 0
i = 0 
with open(r'input_p1.txt', 'r') as f:
    for line in f.readlines():
        instruction = line.split()
        if instruction[0] == "forward":
            x = x + int(instruction[1])
        elif instruction[0] == "down":
            y = y + int(instruction[1])
        elif instruction[0] == "up":
            y = y - int(instruction[1])
        
print(x*y)

