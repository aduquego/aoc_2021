output = []
count=0
sonar=0
with open(r'input_p1.txt', 'r') as f:
    for line in f.readlines():
        output.append(int(line))
        if count >= 3:
            data_new= output[count]+output[count-1]+output[count-2]
            data_old= output[count-1]+output[count-2]+output[count-3]
            if (data_new-data_old)>0:
                sonar= sonar+1
        count= count+1

print(sonar)