
def patern2digit(patterns):
    p2d = {}

    for p, pattern_len in patterns:
        if pattern_len == 2: # Pattern for number 1
            p2d[p] = 1
        elif pattern_len == 3: # Pattern for number 7
            p2d[p] = 7
        elif pattern_len == 4: # Pattern for number 4
            p2d[p] = 4
        elif pattern_len == 7: # Pattern for number 8
            p2d[p] = 8

    d2p = {v: k for k, v in p2d.items()}
    for p, pattern_len in patterns:
        if p in p2d:
            continue
        if pattern_len == 5:  # Pattern for 2, 3 or 5
            if len(p & d2p[1]) == 2: # 3 and 1 have two segments in common
                p2d[p] = 3
            elif len(p & d2p[4]) == 3: # 5 and 4 have three segments in common
                p2d[p] = 5
            else:
                p2d[p] = 2
        else: # Pattenr for 0, 6 or 9
            if len(p & d2p[4]) == 4: # 9 and 5 have four segments in common
                p2d[p] = 9
            elif len(p & d2p[7]) == 2: # 6 and 7 have two segments in common
                p2d[p] = 6
            else:
                p2d[p] = 0
                
    return p2d

sol= 0 
ref_values ={2, 4, 3, 7}

with open(r'input_p1.txt', 'r') as f:
    for text_line in f.readlines():
        patterns, digits = map(str.split, text_line.split('|'))
        patterns = tuple(map(lambda p: (frozenset(p), len(p)), patterns))
        digits = tuple(map(lambda p: (frozenset(p), len(p)), digits))
        p2d = patern2digit(patterns)
        sol += p2d[digits[0][0]] * 1000
        sol += p2d[digits[1][0]] * 100
        sol += p2d[digits[2][0]] * 10
        sol += p2d[digits[3][0]]

print(sol)
