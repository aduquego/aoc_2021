sol= 0 
ref_values ={2, 4, 3, 7}

with open(r'input_p1.txt', 'r') as f:
    for text_line in f.readlines():
        patterns, digits = map(str.split, text_line.split('|'))
        patterns = tuple(map(lambda p: (frozenset(p), len(p)), patterns))
        digits = tuple(map(lambda p: (frozenset(p), len(p)), digits))
        sol += sum(l in ref_values for _, l in digits)

print(sol)
