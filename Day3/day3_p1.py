import pandas as pd

gamma=0
epsilon=0

def split(word):
    return list(word)

def bin2int(bin_list):
    val=0
    for ele in bin_list:
        val = (val << 1) | ele
    return val

output = []
count = 0
with open(r'input_p1.txt', 'r') as f:
    for line in f.readlines():
        data = split(line)
        if count == 0:
            column = len(data)-1
        data_int = list(map(int, data[0:column]))
        output.append(data_int)
        count=count+1

df = pd.DataFrame(output)

gamma_list=df.mode()
epsilon_list = -(gamma_list-1)
gamma_list = gamma_list.values.tolist()
epsilon_list = epsilon_list.values.tolist()

gamma = bin2int(gamma_list[0])
epsilon = bin2int(epsilon_list[0])

print(gamma*epsilon)
        