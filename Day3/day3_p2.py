import pandas as pd

def split(word):
    return list(word)

def bin2int(bin_list):
    val=0
    for ele in bin_list:
        val = (val << 1) | ele
    return val

output = []
count = 0
with open(r'input_p1.txt', 'r') as f:
    for line in f.readlines():
        data = split(line)
        if count == 0:
            column = len(data)-1
        data_int = list(map(int, data[0:column]))
        output.append(data_int)
        count=count+1

df = pd.DataFrame(output)

gamma_list=df.mode()
print(gamma_list)
epsilon_list = -(gamma_list-1)
gamma_list = gamma_list.values.tolist()
epsilon_list = epsilon_list.values.tolist()

gamma = bin2int(gamma_list[0])
epsilon = bin2int(epsilon_list[0])

df_o2 = df

for i in range(df_o2.shape[1]):
    mode = df_o2[i].mode()
    if mode.shape[0] == 2:
        mode = 1
    is_mode = df_o2[i] == int(mode)
    df_o2 = df_o2[is_mode]
    if df_o2.shape[0] == 1:
        break

o2v_list =  df_o2.values.tolist()
o2v = bin2int(o2v_list[0])

df_co2 = df

for i in range(df_co2.shape[1]):
    mode = df_co2[i].mode()
    mode = -(mode -1)
    if mode.shape[0] == 2:
        mode = 0
    is_mode = df_co2[i] == int(mode)
    df_co2 = df_co2[is_mode]
    if df_co2.shape[0] == 1:
        break

co2v_list =  df_co2.values.tolist()
co2v = bin2int(co2v_list[0])

print(o2v*co2v)