from collections import deque

syntax_struct  = str.maketrans('([{<', ')]}>')
syntax_score = {')': 3, ']': 57, '}': 1197, '>': 25137}

def score_calc(s):
	stack = deque()
	for c in s:
		if c in '([{<':
			stack.append(c.translate(syntax_struct))
		elif stack.pop() != c:
			return syntax_score[c]
	return 0

sol = 0

with open(r'input_p1.txt', 'r') as f:
	for line in map(str.rstrip, f):
		score = score_calc(line)
		sol += score


print(sol)
