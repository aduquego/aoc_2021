from collections import deque

syntax_struct  = str.maketrans('([{<', ')]}>')
syntax_score = {')': 3, ']': 57, '}': 1197, '>': 25137}
complement_score  = {')': 1, ']': 2 , '}': 3   , '>': 4    }

def score_calc(s):
    stack = deque()
    for c in s:
        if c in '([{<':
            stack.append(c.translate(syntax_struct))
        elif stack.pop() != c:
            return 0
    
    score = 0
    while stack:
        score *= 5
        score += complement_score[stack.pop()]

    return score

autocomplete=[]
sol = 0

with open(r'input_p1.txt', 'r') as f:
	for line in map(str.rstrip, f):
		score = score_calc(line)
		if score > 0:
		    autocomplete.append(score)

autocomplete.sort()

sol= autocomplete[len(autocomplete) // 2]

print(sol)
