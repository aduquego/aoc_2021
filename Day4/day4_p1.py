import re
import itertools


def Read():
    data = [line.strip() for line in open('input_p1.txt', 'r')]
    data = list(filter(lambda x: x != '', data))
    sequence = data[0]
    sequence = list(map(int, re.findall(r'\d+', sequence)))

    idx = 1
    candidates = []
    while idx < len(data):
        rows = []
        for l in range(0, 5):
            row = list(map(int, re.findall(r'\d+', data[idx+l])))
            rows.append(row)
        candidates.append(rows)
        idx += 5

    return sequence, candidates


def BingoGame(sequence, candidates):
    round = 0
    while True:
        round += 1
        numbers = set(sequence[0:round])
        lastnumber = sequence[round-1]
        for idxt, candidate in enumerate(candidates):
            for row in candidate:
                if set(row).issubset(numbers):
                    return round, lastnumber, idxt
            for col in zip(*candidate):
                if set(col).issubset(numbers):
                    return round, lastnumber, idxt

def ScoreCalc(sequence, candidates, round, lastnumber, winningticket):
    all = set(itertools.chain.from_iterable(candidates[winningticket]))
    notwin = all.difference(set(sequence[0:round]))
    print(sum(notwin)*lastnumber)

sequence, candidates = Read()
round, lastnumber, winningticket = BingoGame(sequence, candidates)
ScoreCalc(sequence, candidates, round, lastnumber, winningticket)
