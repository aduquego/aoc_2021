from collections import defaultdict, Counter

def fuel2(fuel, x):
	sol = 0
	for n in fuel:
		delta = abs(n - x)
		sol += (delta * (delta + 1)) // 2
	return sol

with open(r'input_p1.txt', 'r') as f:
    for text_line in f.readlines():
        data = text_line.split(",")

fuel = list()

for f in data:
	fuel.append(int(f))

fuel.sort()
mean = sum(fuel) // len(fuel)
sol   = min(fuel2(fuel, mean), fuel2(fuel, mean + 1))

print(sol)
