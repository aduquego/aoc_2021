from collections import defaultdict, Counter

with open(r'input_p1.txt', 'r') as f:
    for text_line in f.readlines():
        data = text_line.split(",")

sol = 0
fish = list()
fish_dict = defaultdict(int)

for f in data:
	fish.append(int(f))

for f in fish:
	fish_dict[f] += 1


for _ in range(256):
    new_fish = defaultdict(int)
    for t, n in fish_dict.items():
        t -= 1
        if t < 0:
            new_fish[6] += n
            new_fish[8] += n
        else:
            new_fish[t] += n
        fish_dict = new_fish

sol = sum(fish_dict.values())

print(sol)
