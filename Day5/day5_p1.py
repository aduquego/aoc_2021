from collections import defaultdict

trajectories = defaultdict(int)
with open(r'input_p1.txt', 'r') as f:
    for text_line in f.readlines():
        coordinates_1, coordinates_2 = text_line.split(" -> ")
        x1, y1 = coordinates_1.split(",")
        x2, y2 = coordinates_2.split(",")
        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2)
        y2 = int(y2)
        if x1 == x2:
            for y in range(min(y1, y2), max(y1, y2) + 1):
                trajectories[x1, y] = trajectories[x1, y]+1
        elif y1 == y2:
            for x in range(min(x1, x2), max(x1, x2) + 1):
                trajectories[x, y1] = trajectories[x, y1]+1

sol = sum(point > 1 for point in trajectories.values())
print(sol)
